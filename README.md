# CONSTRUCTOR #
Constructor is a command line JAVA tool which takes two lists, one of a products specifications, and a list of products. It will print out a CSV that generates a mock up an order.

### Options: ###

To specify the *required* product specification file: `-s SPECFILE` 

To specify the *required order file: `-o ORDERFILE`

To specify the optional output file: `-f OUTPUTFILE`

### Build/Run ###

To build:

`gradle completeJar`

`java -jar constructor-all-1.0-SNAPSHOT.jar -s example/specfile.txt -o example/orderlist.txt`

After running the tool, it will generate the output in the same directory as output.csv.

### Output Format ###

`<ORDER ITEM>,<ORDER UUID>,<Part NAME>,<Part UUID>`

## Creating New Spec & Order Files ##

### Input Format ###

Product Specification File: 
`<product name>: <part>, <part>, <part>, <part>`

Order File:

    <product>
    <product>
    <product>