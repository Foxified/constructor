package com.mmodal.bradleyfox;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import static org.mockito.Matchers.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.Assert.*;

/**
 * Created by bfox on 3/2/16.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ConstructorCLI.class, OrderPrinter.class, ProductSpec.class, CustomerOrder.class})
public class ConstructorCLITest {

    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private HashMap<String, ProductSpec> productSpecList = new HashMap<>();
    private ArrayList<CustomerOrder> orderList = new ArrayList<>();

    @Before
    public void setUpStreams() {
        System.setErr(new PrintStream(errContent));
    }

    @After
    public void cleanUpStreams() {
        System.setErr(null);
    }

    @Test
    public void missingSpecFileArgumentMain() throws Exception {
        String[] args = new String[]{"-o order.txt"};
        ConstructorCLI.main(args);
        assertEquals("Missing arguments: -s", errContent.toString().trim());
    }

    @Test
    public void missingOrderFileArgumentMain() throws Exception {
        String[] args = new String[]{"-s spec.txt"};
        ConstructorCLI.main(args);
        assertEquals("Missing arguments: -o", errContent.toString().trim());
    }

    @Test
    public void missingSpecFileAndOrderFileArgumentMain() throws Exception {
        String[] args = new String[]{};
        ConstructorCLI.main(args);
        assertEquals("Missing arguments: -s -o", errContent.toString().trim());
    }

    @Test
    public void usesDefaultOutputFileArgumentMain() throws Exception {
        OrderConstructor orderConstructor = PowerMockito.mock(OrderConstructor.class);

        PowerMockito.mockStatic(OrderPrinter.class);

        PowerMockito.mockStatic(ProductSpec.class);
        PowerMockito.when(ProductSpec.generateFromFile("spec.txt")).thenReturn(productSpecList);

        PowerMockito.mockStatic(CustomerOrder.class);
        PowerMockito.when(CustomerOrder.generateFromFile("order.txt")).thenReturn(orderList);

        PowerMockito.doReturn(orderList).when(orderConstructor).constructOrder();
        PowerMockito.whenNew(OrderConstructor.class).withArguments(productSpecList, orderList).thenReturn(orderConstructor);

        String[] args = new String[]{"-o order.txt", "-s spec.txt"};
        ConstructorCLI.main(args);

        PowerMockito.verifyStatic();
        ProductSpec.generateFromFile("spec.txt");

        PowerMockito.verifyStatic();
        CustomerOrder.generateFromFile("order.txt");

        PowerMockito.verifyStatic();
        OrderPrinter.print("output.csv", orderList);

        PowerMockito.verifyNew(OrderConstructor.class).withArguments(productSpecList, orderList);
    }
}
