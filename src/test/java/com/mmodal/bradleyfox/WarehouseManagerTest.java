package com.mmodal.bradleyfox;

import WidgetFactory.PartNotFoundException;
import WidgetFactory.Warehouse;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;

/**
 * Created by bfox on 3/2/16.
 */
public class WarehouseManagerTest {

    @Test
    public void availableSupplyTest() throws PartNotFoundException {
        Warehouse testWarehouse = Mockito.mock(Warehouse.class);
        WarehouseManager warehouseManager = new WarehouseManager(testWarehouse);
        PowerMockito.when(testWarehouse.available("testPart")).thenReturn(Boolean.valueOf(true));

        warehouseManager.retrieve("testPart");

        Mockito.verify(testWarehouse).retreive("testPart");
    }

    @Test
    public void notOrderingSupplyTest() throws Exception {
        Warehouse testWarehouse = Mockito.mock(Warehouse.class);
        WarehouseManager warehouseManager = new WarehouseManager(testWarehouse);
        PowerMockito.when(testWarehouse.available("testPart")).thenReturn(Boolean.valueOf(true));

        warehouseManager.retrieve("testPart");

        Mockito.verify(testWarehouse, Mockito.never()).order("testPart");
    }

    @Test
    public void orderSupplyTest() throws Exception {
        Warehouse testWarehouse = Mockito.mock(Warehouse.class);
        WarehouseManager warehouseManager = new WarehouseManager(testWarehouse);
        PowerMockito.when(testWarehouse.available("testPart")).thenReturn(Boolean.valueOf(false));

        warehouseManager.retrieve("testPart");

        Mockito.verify(testWarehouse).order("testPart");
    }

}