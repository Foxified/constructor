package com.mmodal.bradleyfox;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.Assert.*;

/**
 * Created by bfox on 3/2/16.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(OrderConstructor.class)
public class OrderConstructorTest {

    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

    private HashMap<String, ProductSpec> productSpecList;
    private ArrayList<CustomerOrder> orderList = new ArrayList<>();

    @Before
    public void setUp() throws Exception {
        ArrayList<String> partList = new ArrayList<>();
        partList.add("wheel");
        productSpecList = new HashMap<>();
        productSpecList.put("van", new ProductSpec(partList));

        System.setErr(new PrintStream(errContent));
    }

    @After
    public void cleanUpStreams() {
        System.setErr(null);
    }

    @Test
    public void completeSpecConstructOrder() throws Exception {
        orderList.add(new CustomerOrder("van"));

        OrderConstructor orderConstructor = new OrderConstructor(productSpecList, orderList);

        ArrayList<CustomerOrder> completedOrderList = orderConstructor.constructOrder();
        CustomerOrder vanOrder = completedOrderList.get(0);

        assertEquals(vanOrder.getItemName(), "van");
        assertFalse(vanOrder.getPartList().isEmpty());
    }

    @Test
    public void missingSpecConstructOrder() throws Exception {
        orderList.add(new CustomerOrder("tricycle"));

        OrderConstructor orderConstructor = new OrderConstructor(productSpecList, orderList);

        ArrayList<CustomerOrder> completedOrderList = orderConstructor.constructOrder();
        CustomerOrder tricycleOrder = completedOrderList.get(0);

        assertEquals(tricycleOrder.getItemName(), "tricycle");
        assertTrue(tricycleOrder.getPartList().isEmpty());
        assertEquals("Missing spec: tricycle", errContent.toString().trim());
    }
}