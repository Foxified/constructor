package com.mmodal.bradleyfox;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.*;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import static org.junit.Assert.*;

/**
 * Created by bfox on 3/2/16.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({FileReader.class, BufferedReader.class, ProductSpec.class})
public class ProductSpecTest {

    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

    @Before
    public void setUpStreams() {
        System.setErr(new PrintStream(errContent));
    }

    @After
    public void cleanUpStreams() {
        System.setErr(null);
    }


    @Test
    public void generateSpecsFromFile() throws Exception {
        FileReader fileReader = PowerMockito.mock(FileReader.class);
        BufferedReader bufferedReader = PowerMockito.mock(BufferedReader.class);

        PowerMockito.whenNew(FileReader.class).withArguments("spec.txt").thenReturn(fileReader);
        PowerMockito.whenNew(BufferedReader.class).withArguments(fileReader).thenReturn(bufferedReader);
        PowerMockito.when(bufferedReader.readLine()).thenReturn("money: paper", "pencil: wood, lead, eraser", null);

        HashMap<String,ProductSpec> productSpecList = ProductSpec.generateFromFile("spec.txt");

        assertEquals(productSpecList.size(), 2);

        ProductSpec moneySpec = productSpecList.get("money");
        ProductSpec pencilSpec = productSpecList.get("pencil");

        assertEquals(moneySpec.getPartList(), new ArrayList<String>(Arrays.asList("paper")));
        assertEquals(pencilSpec.getPartList(), new ArrayList<String>(Arrays.asList("wood", "lead", "eraser")));
    }

    @Test
    public void generateOneSpecFromMultipleInFile() throws Exception {
        FileReader fileReader = PowerMockito.mock(FileReader.class);
        BufferedReader bufferedReader = PowerMockito.mock(BufferedReader.class);

        PowerMockito.whenNew(FileReader.class).withArguments("spec.txt").thenReturn(fileReader);
        PowerMockito.whenNew(BufferedReader.class).withArguments(fileReader).thenReturn(bufferedReader);
        PowerMockito.when(bufferedReader.readLine()).thenReturn("money: paper", "money: sweat, hard work", null);

        HashMap<String,ProductSpec> productSpecList = ProductSpec.generateFromFile("spec.txt");

        ProductSpec moneySpec = productSpecList.get("money");

        assertEquals(productSpecList.size(), 1);
        assertEquals(moneySpec.getPartList(), new ArrayList<String>(Arrays.asList("paper")));
        assertEquals("Conflicting specs: money", errContent.toString().trim());
    }
}