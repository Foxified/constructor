package com.mmodal.bradleyfox;

import WidgetFactory.Part;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by bfox on 3/2/16.
 */
public class OrderPrinter {

    /*
        TODO: Handle different outputs (i.e. to screen)
    */
    public static void print(String filename, ArrayList<CustomerOrder> orderList) {
        try {
            FileWriter writer = new FileWriter(filename);
            for(CustomerOrder order: orderList) {
                for(Part part : order.getPartList()) {
                    writer.append(order.getItemName());
                    writer.append(", " + order.getOrderUUID());
                    writer.append(", " + part.getId());
                    writer.append(", " + part.getSerialNo());
                    writer.append("\n");
                }
            }
            writer.flush();
            writer.close();
        } catch(IOException e)
        {
            e.printStackTrace();
        }
    }
}
