package com.mmodal.bradleyfox;

import WidgetFactory.Part;
import WidgetFactory.PartNotFoundException;
import WidgetFactory.Warehouse;

/**
 * Created by bfox on 3/2/16.
 */
public class WarehouseManager {

    private Warehouse companyWarehouse;

    /*
        If I had further time, I would extend the Warehouse factory and Part to show that the order is still pending.
        It would be an idea to limit the quantity available. With that in mind, we check for availability on any number
        of Parts to ensure that they are available.
        Since the Part gives a Serial Number, the idea is that each Part is unique to the order.
     */
    public Part retrieve(String part) {
        if(!companyWarehouse.available(part)) {
            companyWarehouse.order(part);
        }
        try {
            return companyWarehouse.retreive(part);
        } catch(PartNotFoundException e) {
            return retrieve(part);
        }
    }

    public WarehouseManager(Warehouse companyWarehouse) {
        this.companyWarehouse = companyWarehouse;
    }
}
