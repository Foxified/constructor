package com.mmodal.bradleyfox;

import org.apache.commons.cli.*;
import java.util.ArrayList;
import java.util.HashMap;
import WidgetFactory.Warehouse;

/**
 * Created by bfox on 2/25/16.
 */
public class ConstructorCLI {

    public static void main (String[] args) {
        String specFilename = "";
        String orderFilename = "";
        String outputFilename = "output.csv";
        Options options = new Options();

        options.addOption("s", true, "Spec file");
        options.addOption("o", true, "CustomerOrder file");

        CommandLineParser parser = new DefaultParser();
        try {
            CommandLine cmd = parser.parse(options, args);

            if (cmd.hasOption("s") && (cmd.hasOption("o"))) {
                specFilename = cmd.getOptionValue("s").trim();
                orderFilename = cmd.getOptionValue("o").trim();
            }
            else {
                String message = "";
                if (!cmd.hasOption("s")) { message += "-s "; }
                if (!cmd.hasOption("o")) { message += "-o"; }
                throw new ParseException("Missing arguments: " + message);
            }
            if (cmd.hasOption("f")) {
                outputFilename = cmd.getOptionValue("f");
            }
            HashMap<String, ProductSpec> productList = ProductSpec.generateFromFile(specFilename);
            ArrayList<CustomerOrder> orderList = CustomerOrder.generateFromFile(orderFilename);

            OrderConstructor orderConstructor = new OrderConstructor(productList, orderList);
            orderList = orderConstructor.constructOrder();
            OrderPrinter.print(outputFilename, orderList);
        }
        catch( ParseException exp ) {
            System.err.println(exp.getMessage());
        }

    }

}
