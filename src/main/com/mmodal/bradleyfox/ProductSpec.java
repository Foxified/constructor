package com.mmodal.bradleyfox;

import java.io.FileReader;
import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by bfox on 3/2/16.
 */
public class ProductSpec {

    private ArrayList<String> partList;

    public ArrayList<String> getPartList() {
        return partList;
    }

    public ProductSpec(ArrayList<String> partList) {
        this.partList = partList;
    }

    /*
      TODO: Handle different file format (i.e. CSV)
     */
    public static HashMap<String, ProductSpec> generateFromFile(String filename) {
        {
            HashMap<String, ProductSpec> itemSpecList = new HashMap<>();
            try {
                BufferedReader reader = new BufferedReader(new FileReader(filename));
                String line;
                String[] items;
                while ((line = reader.readLine()) != null) {
                    items = line.split("(:\\s?|,\\s?)");
                    ArrayList<String> itemList= new ArrayList<>(Arrays.asList(items));
                    String product = itemList.remove(0);
                    if(itemSpecList.get(product) == null) {
                        itemSpecList.put(product, new ProductSpec(itemList));
                    } else {
                        System.err.print("Conflicting specs: " + product);
                    }
                }
                reader.close();
                return itemSpecList;
            }
            catch (Exception e) {
                System.err.format("Exception occurred! Failed trying to read '%s'.", filename);
                e.printStackTrace();
                return new HashMap<>();
            }
        }
    }

}
