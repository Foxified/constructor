package com.mmodal.bradleyfox;

import java.util.ArrayList;
import java.util.HashMap;

import WidgetFactory.Part;
import WidgetFactory.Warehouse;


/**
 * Created by bfox on 3/2/16.
 */
public class OrderConstructor {
    private String outputFilename;

    private HashMap<String, ProductSpec> productList;
    private ArrayList<CustomerOrder> orderList;

    /*
        There are two major assumptions based off of the Widget Factory JAR.
        The Warehouse object  that could be requested, and it would not cause a delay.

        The other assumption is that there is no building of parts. For instance, one product being dependent on another
        product being built.
     */
    public ArrayList<CustomerOrder> constructOrder(){
        WarehouseManager manager = new WarehouseManager(new Warehouse());

        orderList.forEach((order) -> {
            ProductSpec product = productList.get(order.getItemName());
            if (product != null) {
                product.getPartList().forEach((part) -> {
                    order.addPart(manager.retrieve(part));
                });
            } else {
                System.err.print("Missing spec: " + order.getItemName());
            }
        });
        return orderList;
    }

    public OrderConstructor(HashMap<String, ProductSpec> productList, ArrayList<CustomerOrder> orderList) {
        this.productList = productList;
        this.orderList = orderList;
    }
}
