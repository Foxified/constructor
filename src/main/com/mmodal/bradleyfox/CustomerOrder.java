package com.mmodal.bradleyfox;

import WidgetFactory.Part;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;

/**
 * Created by bfox on 3/2/16.
 */
public class CustomerOrder {

    private UUID orderUUID;

    private String itemName = "";

    private ArrayList<Part> partList;

    public UUID getOrderUUID() {
        return orderUUID;
    }

    public String getItemName() {
        return itemName;
    }

    public ArrayList<Part> getPartList() {
        return partList;
    }

    public void addPart(Part part) {
        this.partList.add(part);
    }

    public CustomerOrder(String itemName) {
        this.orderUUID = UUID.randomUUID();
        this.itemName = itemName;
        this.partList = new ArrayList<>();
    }

    /*
        TODO: Handle different file format (i.e. CSV)
     */
    public static ArrayList<CustomerOrder> generateFromFile(String filename) {
        {
            ArrayList<CustomerOrder> clientOrder = new ArrayList<>();
            try {
                BufferedReader reader = new BufferedReader(new java.io.FileReader(filename));
                String line;
                while ((line = reader.readLine()) != null) {
                    clientOrder.add(new CustomerOrder(line));
                }
                reader.close();
                return clientOrder;
            }
            catch (Exception e) {
                System.err.format("Exception occurred trying to read '%s'.", filename);
                e.printStackTrace();
                return new ArrayList<CustomerOrder>();
            }
        }
    }
}
